#!/bin/bash

usage() {
    cat <<EOF
NAME
    vm reboot - reboot a virtual machine

SYNOPSIS
    vm reboot name
EOF
    exit 1
}

# parse positional parameter
parsepositional() {
    local pos="$1"
    local arg="$2"

    case "$pos" in
        x)
            NAME="$arg"
            ;;
        *)
            echo 1>&2 "unknown argument: '$arg'"
            exit 1
            ;;
    esac
}

# parse arguments
parseargs() {
    local pos="x"
    local i
    local tmp

    while [ $# -ge 1 ] ; do
        case "$1" in
            --help)
                usage
                ;;
            --)
                shift
                break
                ;;
            --*)
                echo 1>&2 "unknown option '$1' - ignored."
                ;;
            *)
                parsepositional "$pos" "$1"
                pos="x$pos"
                ;;
        esac
        shift
    done

    # parse remaining positional arguments
    while [ $# -ge 1 ] ; do
        parsepositional "$pos" "$1"
        pos="x$pos"
        shift
    done

    # check if all positional arguments where provided
    if [ "$pos" != "xx" ] ; then
        echo 1>&2 "missing/insufficient arguments."
        echo
        usage
    fi
}

main() {
    # parse arguments
    parseargs "$@"

    virsh reboot "$NAME"
}