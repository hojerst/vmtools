#!/bin/bash

usage() {
    cat <<EOF
NAME
    list - list vms

SYNOPSIS
    list [options]

OPTIONS
    --all,-a    also list stopped vms
EOF
    exit 1
}

# parse arguments
parseargs() {
    local pos="x"
    local i
    local tmp

    while [ $# -ge 1 ] ; do
        case "$1" in
            --all|-a)
                LIST_ALL=y
                ;;
            --help)
                usage
                ;;
            --)
                shift
                break
                ;;
            --*)
                echo 1>&2 "unknown option '$1' - ignored."
                ;;
            *)
                parsepositional "$pos" "$1"
                pos="x$pos"
                ;;
        esac
        shift
    done

    # parse remaining positional arguments
    while [ $# -ge 1 ] ; do
        parsepositional "$pos" "$1"
        pos="x$pos"
        shift
    done

    # check if all positional arguments where provided
    if [ "$pos" != "x" ] ; then
        echo 1>&2 "missing/insufficient arguments."
        echo
        usage
    fi
}

main() {
    parseargs "$@"

    if [ "$LIST_ALL" = "y" ] ; then
        virsh list --all
    else
        virsh list
    fi
}