#!/bin/bash

usage() {
    cat <<EOF
NAME
    images - list available images

SYNOPSIS
    vm images
EOF
    exit 1
}

# parse arguments
parseargs() {
    local pos="x"
    local i
    local tmp

    while [ $# -ge 1 ] ; do
        case "$1" in
            --help)
                usage
                ;;
            --)
                shift
                break
                ;;
            --*)
                echo 1>&2 "unknown option '$1' - ignored."
                ;;
            *)
                parsepositional "$pos" "$1"
                pos="x$pos"
                ;;
        esac
        shift
    done

    # parse remaining positional arguments
    while [ $# -ge 1 ] ; do
        parsepositional "$pos" "$1"
        pos="x$pos"
        shift
    done

    # check if all positional arguments where provided
    if [ "$pos" != "x" ] ; then
        echo 1>&2 "missing/insufficient arguments."
        echo
        usage
    fi
}

main() {
    parseargs "$@"
    list_images
}